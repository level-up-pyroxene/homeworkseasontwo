package org.levelup.annotations;

public class RussianRoulette {

    @RandomInt(min = 1, max = 6)
    private int number;
//    @RandomInt(min = 1, max = 2)
    private double withoutAnnotation;


    public void guess(int number) {
        if (number == this.number) {
            System.out.println("You died...");
        }
    }

    public int getNumber() {
        return number;
    }
}
