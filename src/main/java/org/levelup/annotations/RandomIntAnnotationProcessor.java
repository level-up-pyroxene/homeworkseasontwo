package org.levelup.annotations;

import org.h2.store.fs.FileUtils;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class RandomIntAnnotationProcessor {

    private static final String JAVA_EXTENSION = ".java";
    private static final String SOURCE_ROOT = "src/main/java";


    public static void setField(Object object) throws IllegalAccessException, AnnotationInvalidFieldTypeException {
        // 1. Получаем объекта класса Class
        // 2. Находим все его поля
        // 3. Находим аннтоции RandomInt
        // 4. Устанавливаем значение

        // RussianRoulette.class
        // object - String -> Class<String>
        // object - Phone -> Class<Phone>
        // object - Shape -> Class<Shape>
        Class<?> objectClass = object.getClass();
        Field[] fields = objectClass.getDeclaredFields();

        for (Field field : fields) {
            // Class<RandomInt>
//            System.out.println("Field: " + field.getName());

//            System.out.println("Field type: " + fieldType);
//            System.out.println("Is null: " + (annotation == null));
            Class<?> fieldType = field.getType();
            RandomInt annotation = field.getAnnotation(RandomInt.class);
            if (annotation != null) {
                if(fieldType != Integer.TYPE){
                    throw new AnnotationInvalidFieldTypeException();
                }
                // Math.random()
                // [1 .. 6]
                // nextInt(6) - [0 .. 5]
                // max - 6, min - 1
                // max - min - [0 .. 5)
                // max - min + 1 [0 .. 6)
                // (max - min + 1) + 1 -> [1 .. 7)
                int number = new Random()
                        .nextInt(annotation.max() - annotation.min() + 1) + annotation.min();
                System.out.println("Number: " + number);
                // field - number
                // first parameter - object
                // second parameter - value
                field.setAccessible(true);
                field.set(object, number);
            }
        }

    }


    public static void setField(List<Class<?>> classesList) throws IllegalAccessException, InstantiationException {
        for (Class<?> tempClass : classesList) {
            if(!tempClass.isAnnotation()) {
                Object tempObject = tempClass.newInstance();
                try {
                    setField(tempObject);

                } catch (AnnotationInvalidFieldTypeException e) {

                }
            }

        }
    }

    public static List<Class<?>> getFilesListed(String string) throws IOException {
        Path inputPath = Paths.get(string);
        if (!Files.exists(inputPath) || !Files.isDirectory(inputPath)) {
            return Collections.emptyList();
        }

        Path sourceRootPath = Paths.get(SOURCE_ROOT).toAbsolutePath();
        return collectFromDirectory(sourceRootPath, inputPath);
    }

    private static List<Class<?>> collectFromDirectory(Path sourceRoot, Path directoryPath) throws IOException {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        return Files.walk(directoryPath)
                .filter(Files::isRegularFile)
                .filter(p -> p.toString().endsWith(JAVA_EXTENSION))
                .map(sourceRoot::relativize)
                .map(Path::toString)
                .map(s -> s.substring(0, s.length() - JAVA_EXTENSION.length()))
                .map(s -> s.replaceAll("[\\\\/]", "."))
                .flatMap(s -> tryLoadClass(classLoader, s))
                .collect(Collectors.toList());
    }

    private static Stream<Class<?>> tryLoadClass(ClassLoader classLoader, String name) {
        try {
            System.out.println("Name of loaded class: " + name);
            return Stream.of(classLoader.loadClass(name));
        } catch (ClassNotFoundException e) {
            System.out.println("Class not found by name: " + name);
            return Stream.empty();
        }
    }
}
