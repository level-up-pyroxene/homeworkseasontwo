//package org.levelup.hibernate.repository.impl;
//
//import org.hibernate.SessionFactory;
//import org.junit.jupiter.api.*;
//import org.levelup.hibernate.domain.Film;
//import org.levelup.hibernate.repository.FilmRepository;
//
//import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.junit.jupiter.api.Assertions.assertNotNull;
//
//public class FilmRepositoryImplIT {
//
//    private static SessionFactory factory;
//    private FilmRepository filmRepository;
//
//    @BeforeAll
//    public static void setupFactory() {
//        factory = TestHibernateConfiguration.getFactory();
//    }
//
//    @BeforeEach
//    public void setup() {
//        this.filmRepository = new FilmRepositoryImpl(factory);
//    }
//
//    @Test
//    public void testCreate() {
//        int id = 100;
//        String name = "film name";
//        double duration = 165.4d;
//        String genre = "genre";
//        int year = 2012;
//
//        filmRepository.create(id, name, duration, genre, year);
//        Film filmFromDB = filmRepository.getById(id);
//
//        assertEquals(id, filmFromDB.getFilmId());
//        assertEquals(name, filmFromDB.getName());
//        assertEquals(duration, filmFromDB.getDuration());
//        assertEquals(genre, filmFromDB.getGenre());
//        assertEquals(year, filmFromDB.getYear());
//    }
//
//    @Test
//    public void testGetById() {
//        int id = 101;
//        filmRepository.create(id, "", 0d, "", 0);
//
//        Film film = filmRepository.getById(id);
//        assertNotNull(film);
//    }
//
//    @AfterAll
//    public static void closeFactory() {
//        factory.close();
//    }
//
//}
