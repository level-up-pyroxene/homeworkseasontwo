package org.levelup.annotations;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class RandomIntAnnotationTest {

    RussianRoulette roulette = new RussianRoulette();

    @Test
    @DisplayName("is Type is correct")
    public void testAnnotation_passNotValidType_throwsInvalidTypeExc() {
        Assertions.assertThrows(AnnotationInvalidFieldTypeException.class, () -> RandomIntAnnotationProcessor .setField(roulette));
    }

    @Test
    @DisplayName("Test if AnnotationsProcessor sets the value")
    public void testRandomIntAnnotationProcessor_setField_fieldIsSet(){

        Assertions.assertTrue(roulette.getNumber()>=0);
    }

}
