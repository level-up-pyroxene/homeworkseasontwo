package org.levelup.films;

import org.junit.jupiter.api.*;

import java.sql.Connection;
import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.*;

public class JdbcUtilsIntegrationTest {

    private Connection connection;

    @Test
    @DisplayName("Open new connection to DB")
    public void testCreateConnection_returnCreatedConnection() throws SQLException {
        connection = JdbcUtils.createConnection();
        assertNotNull(connection);
        assertFalse(connection.isClosed());
    }

    @BeforeEach
    @DisplayName("open connection")
    public void openConnection() throws SQLException {connection = JdbcUtils.createConnection(); }

    @Test
    @DisplayName("Print all data from film")
    public void testSelectQuery_validQuery_printResultSet() throws SQLException {

        String testQuery = "select * from film inner join film_detail where film.id = film_detail.film_id;";
        String testQueryTwo = "select * from film";
        String testQueryThree = "select * from film where name = \"Pulp Fiction\";";
        String testQueryFour = "select * from film_detail";

        JdbcUtils.selectQuery(connection, testQueryTwo);
    }

    @Test
    @DisplayName("Update filed result not null")
    public void testUpdateField_whenWorks_resultNotNull() throws SQLException {

        String testQuery = "update film set duration = 888 where id = 5;";

        JdbcUtils.updateQuery(connection, testQuery);
        System.out.println();

    }

    @AfterEach
    public void closeConnection() throws SQLException {
        connection.close();
    }

}
